/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foplab2;

import java.util.Scanner;

/**
 *
 * @author Brandon Tan Zhirong
 */
public class Q2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        double P = s.nextDouble();
        double D = s.nextDouble();
        double R = s.nextDouble();
        double Y = s.nextDouble();
        double m = (P - D) * (1 + R*Y/100) / (Y *12);
        System.out.println("Monthly Payment is " + m);
    }
    
}
