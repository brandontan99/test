/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foplab2;

import java.util.Scanner;

/**
 *
 * @author Brandon Tan Zhirong
 */
public class Q4 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the number of seconds: ");
        long uerInput = s.nextLong();
        long min = (uerInput/60)%60;
        long hour = uerInput/60/60;
        long sec = uerInput%60;
        System.out.println(uerInput +" seconds is "+ 
                hour +" hours, "+
                min + " minutes and "+ sec + " seconds");
    }
}
